#include <stdio.h>

int main(void)
{
    int array[10];
    array[0] = 5;
    array[1] = 9;
    array[2] = 10;
    array[3] = 6;
    array[4] = 7;
    array[5] = 4;
    array[6] = 2;
    array[7] = 1;
    array[8] = 3;
    array[9] = 8;
    int aenderungen = 0;
    for (int i = 0; i < 10; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");

    do
    {
        aenderungen = 0;

        for (int i = 0; i < 9; i++)
        {
            if (array[i] > array[i + 1])
            {
                int swap = 0;
                swap = array[i];
                array[i] = array[i + 1];
                array[i + 1] = swap;

                aenderungen++;
            }
        }

        for (int i = 0; i < 10; i++)
        {
            printf("%d ", array[i]);
        }
        printf("\n");

    } while (aenderungen > 0);

    return 0;
}